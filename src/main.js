import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'

// 引入全局样式表
import './assets/css/global.css'
// 导入第三方树形表格
import ZkTable from 'vue-table-with-tree-grid'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器所需样式
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
// 导入 Nprogress 进度条的js和css
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 全局注册第三方树形表格
Vue.component('tree-table', ZkTable)
// 全局注册使用富文本编辑器
Vue.use(VueQuillEditor)

// 导入axios用于发起网络接口请求
import axios from 'axios'
// 配置接口基准地址
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
// 配置axios请求拦截器
axios.interceptors.request.use(config => {
  // 发送请求时显示进度条
  NProgress.start();
  // 给每一次请求的添加请求头
  config.headers.Authorization = window.sessionStorage.getItem('token');
  return config;
})
// 配置response响应拦截器
axios.interceptors.response.use(config => {
  // 响应完成隐藏进度条
  NProgress.done();
  return config;
})
// 将axios挂载在Vue原型上
Vue.prototype.$http = axios

Vue.config.productionTip = false

// 日期过滤器
Vue.filter('dateFormat', function (originVal) {
  // 形参 originVal 乘以 1000 ， 防止年份为 1970
  const dt = new Date(originVal * 1000)
  // padStart(2, '0') 为补零操作
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')

  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')

  return `${y} -${m} -${d} ${hh}:${mm}:${ss}`
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
